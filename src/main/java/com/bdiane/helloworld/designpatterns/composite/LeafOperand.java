/**
 * Copyright (c) 2018 bdiane.helloworld
 * All rights reserved
 */
package com.bdiane.helloworld.designpatterns.composite;

public class LeafOperand implements ArithmeticExpression {

    private final int value;

    public LeafOperand(int value) {
        this.value = value;
    }

    @Override
    public String print() {
        return String.valueOf(value);
    }

    @Override
    public int evaluate() {
        return value;
    }
}
