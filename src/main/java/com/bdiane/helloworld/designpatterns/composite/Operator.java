/**
 * Copyright (c) 2018 bdiane.helloworld
 * All rights reserved
 */
package com.bdiane.helloworld.designpatterns.composite;

public enum Operator {

    ADDITION("+"), SUBTRACTION("-"), MULTIPLICATION("*"), DIVISION("/");

    private final String name;

    Operator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
