/**
 * Copyright (c) 2018 bdiane.helloworld
 * All rights reserved
 */
package com.bdiane.helloworld.designpatterns.composite;

public class CompositeOperand implements ArithmeticExpression {

    private final Operator operator;
    private final ArithmeticExpression leafOperand;
    private final ArithmeticExpression rightOperand;

    public CompositeOperand(ArithmeticExpression operand, Operator operator) {
        this.leafOperand = operand;
        this.rightOperand = null;
        this.operator = operator;
    }

    public CompositeOperand(ArithmeticExpression leafOperand, ArithmeticExpression rightOperand, Operator operator) {
        this.leafOperand = leafOperand;
        this.rightOperand = rightOperand;
        this.operator = operator;
    }

    @Override
    public String print() {
        return leafOperand.print() + operator.getName() + rightOperand.print();
    }

    @Override
    public int evaluate() {
        switch (operator) {
            case ADDITION:
                return leafOperand.evaluate() + rightOperand.evaluate();
            case SUBTRACTION:
                return leafOperand.evaluate() - rightOperand.evaluate();
            case MULTIPLICATION:
                return leafOperand.evaluate() * rightOperand.evaluate();
            case DIVISION:
                return leafOperand.evaluate() / rightOperand.evaluate();
            default:
                return 0;
        }
    }
}
