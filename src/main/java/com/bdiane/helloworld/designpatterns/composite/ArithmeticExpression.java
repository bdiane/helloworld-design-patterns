/**
 * Copyright (c) 2018 bdiane.helloworld
 * All rights reserved
 */
package com.bdiane.helloworld.designpatterns.composite;

public interface ArithmeticExpression {

    String print();
    int evaluate();
}
