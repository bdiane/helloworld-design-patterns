/**
 * Copyright (c) 2018 bdiane.helloworld
 * All rights reserved
 */
package com.bdiane.helloworld.designpatterns.composite;

import static com.bdiane.helloworld.designpatterns.composite.Operator.ADDITION;
import static com.bdiane.helloworld.designpatterns.composite.Operator.DIVISION;
import static com.bdiane.helloworld.designpatterns.composite.Operator.MULTIPLICATION;
import static com.bdiane.helloworld.designpatterns.composite.Operator.SUBTRACTION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ArithmeticExpressionTest {

    private static final int VALUE_OF_A = 2;
    private static final int VALUE_OF_B = 1;
    private static final int VALUE_OF_C = 5;

    private static final String PRINT_OF_A_PLUS_B;
    private static final String PRINT_OF_B_MINUS_C;
    private static final String PRINT_OF_A_MULTIPLICATION_B;
    private static final String PRINT_OF_A_DIVISION_B;

    private static final int EVALUATION_OF_A_PLUS_B;
    private static final int EVALUATION_OF_B_MINUS_C;
    private static final int EVALUATION_OF_A_MULTIPLICATION_B;
    private static final int EVALUATION_OF_A_DIVISION_B;

    static {
        PRINT_OF_A_PLUS_B = VALUE_OF_A + ADDITION.getName() + VALUE_OF_B;
        PRINT_OF_B_MINUS_C = VALUE_OF_B + SUBTRACTION.getName() + VALUE_OF_C;
        PRINT_OF_A_MULTIPLICATION_B = VALUE_OF_A + MULTIPLICATION.getName() + VALUE_OF_B;
        PRINT_OF_A_DIVISION_B = VALUE_OF_A + DIVISION.getName() + VALUE_OF_B;
        EVALUATION_OF_A_PLUS_B = VALUE_OF_A + VALUE_OF_B;
        EVALUATION_OF_B_MINUS_C = VALUE_OF_B - VALUE_OF_C;
        EVALUATION_OF_A_MULTIPLICATION_B = VALUE_OF_A * VALUE_OF_B;
        EVALUATION_OF_A_DIVISION_B = VALUE_OF_A / VALUE_OF_B;
    }

    private ArithmeticExpression a;
    private ArithmeticExpression b;
    private ArithmeticExpression c;
    private ArithmeticExpression a_plus_b;
    private ArithmeticExpression b_minus_c;
    private ArithmeticExpression a_multiplication_b;
    private ArithmeticExpression a_division_b;

    @BeforeEach
    void beforeEach() {
        a = new LeafOperand(VALUE_OF_A);
        b = new LeafOperand(VALUE_OF_B);
        c = new LeafOperand(VALUE_OF_C);
    }

    @Test
    void testPrintAddition() {
        arangeAdditionTest();
        String a_plus_b_print = a_plus_b.print();
        assertEquals(PRINT_OF_A_PLUS_B, a_plus_b_print);
    }

    @Test
    public void testEvaluateAddition() {
        arangeAdditionTest();
        int a_plus_b_value = a_plus_b.evaluate();
        assertEquals(EVALUATION_OF_A_PLUS_B, a_plus_b_value);
    }

    private void arangeAdditionTest() {
        a_plus_b = new CompositeOperand(a, b, ADDITION);
    }

    @Test
    void testPrintSubtraction() {
        arangeSubtractionTest();
        String a_plus_b_minus_c_print = b_minus_c.print();
        assertEquals(PRINT_OF_B_MINUS_C, a_plus_b_minus_c_print);
    }

    @Test
    public void testEvaluateSubtraction() {
        arangeSubtractionTest();
        int a_plus_b_minus_c_value = b_minus_c.evaluate();
        assertEquals(EVALUATION_OF_B_MINUS_C, a_plus_b_minus_c_value);
    }

    private void arangeSubtractionTest() {
        b_minus_c = new CompositeOperand(b, c, SUBTRACTION);
    }

    @Test
    void testPrintMultiplication() {
        arangeMultiplicationTest();
        String a_multiplied_by_b_print = a_multiplication_b.print();
        assertEquals(PRINT_OF_A_MULTIPLICATION_B, a_multiplied_by_b_print);
    }

    @Test
    public void testEvaluateMultiplication() {
        arangeMultiplicationTest();
        int a_multiplied_by_b_value = a_multiplication_b.evaluate();
        assertEquals(EVALUATION_OF_A_MULTIPLICATION_B, a_multiplied_by_b_value);
    }

    private void arangeMultiplicationTest() {
        a_multiplication_b = new CompositeOperand(a, b, MULTIPLICATION);
    }

    @Test
    void testPrintDivision() {
        arangeDivisionTest();
        String a_divided_by_b_print = a_division_b.print();
        assertEquals(PRINT_OF_A_DIVISION_B, a_divided_by_b_print);
    }

    @Test
    public void testEvaluateDivision() {
        arangeDivisionTest();
        int a_divided_by_b_value = a_division_b.evaluate();
        assertEquals(EVALUATION_OF_A_DIVISION_B, a_divided_by_b_value);
    }

    private void arangeDivisionTest() {
        a_division_b = new CompositeOperand(a, b, DIVISION);
    }
}
